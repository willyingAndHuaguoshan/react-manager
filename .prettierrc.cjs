module.exports = {
  printWith: 120, //行宽度超过120换行
  useTabs: false,
  tabWidth: 2, // 缩进
  semi: true, //结尾使用分号
  singleQuote: true, // jsx使用单引号
  jsxSingleQuote: true, // jsx使用单引号
  arrowParens: 'avoid', // 如果是一个参数的时候，取消括号
  bracketSpacing: true, // 对象或者数组{a: 5} { a: 5 }
  trailingComma: 'none' //尾随逗号
};
