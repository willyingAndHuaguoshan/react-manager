import React, { useEffect } from 'react';
import { Layout } from 'antd';
import NavHeader from '@/components/NavBar';
import NavFooter from '@/components/NavFooter';
import ManagerMenu from '@/components/Menu';
import {
  Outlet,
  useRouteLoaderData,
  useLocation,
  Navigate
} from 'react-router-dom';
import styles from './index.module.less';
import { getUserInfo } from '@/api';
import useStore from '@/store';
import { IuseRouterData } from '@/types';
import { getTargetRouter } from '@/utils';
import { router } from '@/router';
const { Header, Sider, Content } = Layout;

const ManagerLayout: React.FC = () => {
  const collapsed = useStore(state => state.collapsed);
  const updataUserInfo = useStore(state => state.updataUserInfo);
  const { pathname } = useLocation();

  useEffect(() => {
    getUserInfo().then(res => {
      updataUserInfo(res);
    });
  }, []);

  const data = useRouteLoaderData('layout') as IuseRouterData;
  const targetRouter = getTargetRouter(pathname, router);
  if (targetRouter && targetRouter.meta?.permission === false) {
    // 继续
  } else {
    const staticRoutes = ['/welcome', '/403', '/404'];
    if (
      !data.menuPathList.includes(pathname) &&
      !staticRoutes.includes(pathname)
    ) {
      return <Navigate to='/403'></Navigate>;
    }
  }
  return (
    <Layout>
      <Sider trigger={null} collapsed={collapsed} collapsible>
        <ManagerMenu></ManagerMenu>
      </Sider>
      <Layout>
        <Header style={{ background: '#fff', padding: 0 }}>
          <NavHeader></NavHeader>
        </Header>
        <Content className={styles['content']}>
          <div className='wrapper'>
            <Outlet></Outlet>
          </div>
          <NavFooter></NavFooter>
        </Content>
      </Layout>
    </Layout>
  );
};

export default ManagerLayout;
