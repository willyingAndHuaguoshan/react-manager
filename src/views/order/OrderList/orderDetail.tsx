import { Descriptions, Modal } from 'antd';
import { useImperativeHandle, useState } from 'react';
import { orderDetail } from '@/api';
import { IModalDetailProps } from '@/types/modal';
import { Order } from '@/types';
import { encryptMobile, formatMoney, formatDate } from '@/utils';
export default function OrderDetail(props: IModalDetailProps) {
  const [visiable, setVisiable] = useState(false);
  const [orderData, setOrderData] = useState<Order.OrderItem>();
  const handleCloseModal = () => {
    setVisiable(false);
  };
  const open = async (id: string) => {
    setVisiable(true);
    const data = await orderDetail(id);
    setOrderData(data);
  };
  useImperativeHandle(props.mRef, () => ({
    open
  }));
  return (
    <Modal
      title='订单详情'
      width={950}
      open={visiable}
      // footer={false}
      onCancel={handleCloseModal}
      onOk={handleCloseModal}
      okText='确认'
      cancelText='取消'
    >
      <Descriptions column={2} style={{ padding: '10px 30px' }}>
        <Descriptions.Item label='订单编号'>
          {orderData?.orderId}
        </Descriptions.Item>
        <Descriptions.Item label='下单城市'>
          {orderData?.cityName}
        </Descriptions.Item>
        <Descriptions.Item label='下单用户'>
          {orderData?.userName}
        </Descriptions.Item>
        <Descriptions.Item label='手机号'>
          {encryptMobile(orderData?.mobile)}
        </Descriptions.Item>
        <Descriptions.Item label='起点'>
          {orderData?.startAddress}
        </Descriptions.Item>
        <Descriptions.Item label='终点'>
          {orderData?.endAddress}
        </Descriptions.Item>
        <Descriptions.Item label='订单金额'>
          {formatMoney(orderData?.orderAmount)}
        </Descriptions.Item>
        <Descriptions.Item label='用户支付金额'>
          {formatMoney(orderData?.userPayAmount)}
        </Descriptions.Item>
        <Descriptions.Item label='司机到账金额'>
          {orderData?.dirverAmount}
        </Descriptions.Item>
        <Descriptions.Item label='支付方式'>
          {orderData?.payType}
        </Descriptions.Item>
        <Descriptions.Item label='司机名称'>
          {orderData?.driverName}
        </Descriptions.Item>
        <Descriptions.Item label='订单车型'>
          {orderData?.vehicleName}
        </Descriptions.Item>
        <Descriptions.Item label='订单状态'>
          {orderData?.state}
        </Descriptions.Item>
        <Descriptions.Item label='用车时间'>
          {formatDate(orderData?.useTime)}
        </Descriptions.Item>
        <Descriptions.Item label='订单结束时间'>
          {formatDate(orderData?.endTime)}
        </Descriptions.Item>
        <Descriptions.Item label='订单创建时间'>
          {formatDate(orderData?.createTime)}
        </Descriptions.Item>
      </Descriptions>
    </Modal>
  );
}
