import { IModalProps } from '@/types/modal';
import { Modal, Form, Select, Input, DatePicker, Row, Col } from 'antd';
import { useEffect, useImperativeHandle, useState } from 'react';
import { useForm } from 'antd/es/form/Form';
import { getCityList, getCarTypeList, createOrder } from '@/api';
import { Order } from '@/types';
import { message } from '@/utils/AntdGlobal';

export default function CreateOrderList(props: IModalProps) {
  const [visiable, setVisiable] = useState(false);
  const [cityList, setCityList] = useState<Order.CityItem[]>([]);
  const [vehicleList, setVehicleList] = useState<Order.VehicleItem[]>([]);
  const [form] = useForm();
  useImperativeHandle(props.mRef, () => ({ open }));
  const open = () => {
    setVisiable(true);
  };
  const handleCancel = () => {
    setVisiable(false);
  };
  const getCityListData = async () => {
    const cityList = await getCityList();
    setCityList(cityList);
  };
  const getVehicleListData = async () => {
    const vehicleList = await getCarTypeList();
    setVehicleList(vehicleList);
  };
  const handleSubmit = async () => {
    const valid = await form.validateFields();
    if (valid) {
      await createOrder(form.getFieldsValue());
      message.success('创建订单成功');
      props.update();
      setVisiable(false);
      form.resetFields();
    }
  };
  useEffect(() => {
    getCityListData();
    getVehicleListData();
  }, []);
  return (
    <Modal
      title='创建订单'
      open={visiable}
      onCancel={handleCancel}
      onOk={handleSubmit}
      width={800}
      okText='确定'
      cancelText='取消'
    >
      <Form
        form={form}
        layout='horizontal'
        labelAlign='right'
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{
          payType: '微信',
          state: 1
        }}
      >
        <Row>
          <Col span={12}>
            <Form.Item
              label='城市名称'
              name='cityName'
              rules={[
                {
                  required: true,
                  message: '请选择城市'
                }
              ]}
            >
              <Select placeholder='请选择城市'>
                {cityList?.map(city => (
                  <Select.Option value={city.name} key={city.id}>
                    {city.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='车型'
              name='vehicleName'
              rules={[
                {
                  required: true,
                  message: '请选择车型'
                }
              ]}
            >
              <Select placeholder='请选择车型'>
                {vehicleList?.map(vehicle => {
                  return (
                    <Select.Option value={vehicle.name} key={vehicle.id}>
                      {vehicle.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <Form.Item
              label='用户名称'
              name='userName'
              rules={[
                {
                  required: true,
                  message: '请输入用户名称'
                }
              ]}
            >
              <Input placeholder='请输入用户名称'></Input>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label='手机号' name='mobile'>
              <Input placeholder='请输入手机号' type='number'></Input>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <Form.Item label='起始地址' name='startAddress'>
              <Input placeholder='请输入起始地址'></Input>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label='结束地址' name='endAddress'>
              <Input placeholder='请输入结束地址'></Input>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <Form.Item
              label='下单金额'
              name='orderAmount'
              rules={[
                {
                  required: true,
                  message: '请输入下单金额'
                }
              ]}
            >
              <Input placeholder='请输入下单金额' type='number'></Input>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='支付金额'
              name='userpayAmount'
              rules={[
                {
                  required: true,
                  message: '请输入支付金额'
                }
              ]}
            >
              <Input placeholder='请输入支付金额' type='number'></Input>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <Form.Item
              label='司机名称'
              name='driverName'
              rules={[
                {
                  required: true,
                  message: '请输入司机名称'
                }
              ]}
            >
              <Input placeholder='请输入司机名称'></Input>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='司机金额'
              name='driverAmount'
              rules={[
                {
                  required: true,
                  message: '请输入司机金额'
                }
              ]}
            >
              <Input placeholder='请输入司机金额'></Input>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <Form.Item label='支付方式' name='payType'>
              <Select placeholder='请选择支付方式'>
                <Select.Option value={1}>微信</Select.Option>
                <Select.Option value={2}>支付宝</Select.Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label='订单状态' name='state'>
              <Select placeholder='请选择订单状态'>
                <Select.Option value={1}>进行中</Select.Option>
                <Select.Option value={2}>已完成</Select.Option>
                <Select.Option value={3}>超时</Select.Option>
                <Select.Option value={4}>取消</Select.Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <Form.Item label='用车时间' name='useTime'>
              <DatePicker />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label='结束时间' name='endTime'>
              <DatePicker />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
}
