import { Menu } from '@/types';
import { IAction, IModalProps } from '@/types/modal';
import { Modal, Form, Input, TreeSelect, InputNumber, Radio } from 'antd';
import { useEffect, useImperativeHandle, useState } from 'react';
import { useForm } from 'antd/es/form/Form';
import { getMenuList, createMenu, editMenu } from '@/api';
import { message } from '@/utils/AntdGlobal';
import { InfoCircleOutlined } from '@ant-design/icons';

export default function CreateMenu(props: IModalProps<Menu.EditParams>) {
  const [action, setAction] = useState<IAction>('create');
  const [visiable, setVisiable] = useState(false);
  const [menuList, setMenuList] = useState<Menu.MenuItem[]>();
  const [form] = useForm();

  useEffect(() => {
    getMenuData();
  }, []);

  useImperativeHandle(props.mRef, () => ({
    open
  }));

  const handleSubmit = async () => {
    const valid = await form.validateFields();
    if (valid) {
      if (action === 'create') {
        await createMenu(form.getFieldsValue());
        message.success('创建成功');
      } else {
        await editMenu(form.getFieldsValue());
        message.success('编辑成功');
      }
      form.resetFields();
      setVisiable(false);
      props.update();
    }
  };
  const handleCancel = () => {
    setVisiable(false);
    form.resetFields();
  };
  const getMenuData = async () => {
    const data = await getMenuList();
    setMenuList(data);
  };
  const open = (
    type: IAction,
    data?: Menu.EditParams | { parentId?: string; orderB?: number }
  ) => {
    if (data) {
      form.setFieldsValue(data);
    }
    setAction(type);
    setVisiable(true);
  };
  return (
    <Modal
      title={action === 'create' ? '新增部门' : '编辑部门'}
      width={800}
      open={visiable}
      okText='确定'
      cancelText='取消'
      onOk={handleSubmit}
      onCancel={handleCancel}
    >
      <Form
        form={form}
        labelAlign='right'
        labelCol={{ span: 4 }}
        initialValues={{
          menuType: 1,
          menuState: 1
        }}
      >
        <Form.Item name='_id' hidden>
          <Input></Input>
        </Form.Item>
        <Form.Item label='上级部门' name='parentId'>
          <TreeSelect
            placeholder='请选择父级菜单'
            allowClear
            treeDefaultExpandAll
            fieldNames={{
              label: 'menuName',
              value: '_id'
            }}
            treeData={menuList}
            showCheckedStrategy={TreeSelect.SHOW_ALL}
          ></TreeSelect>
        </Form.Item>
        <Form.Item label='菜单类型' name='menuType'>
          <Radio.Group>
            <Radio value={1}>菜单</Radio>
            <Radio value={2}>按钮</Radio>
            <Radio value={3}>页面</Radio>
          </Radio.Group>
        </Form.Item>
        <Form.Item
          label='菜单名称'
          name='menuName'
          rules={[{ required: true, message: '请输入菜单名称' }]}
        >
          <Input placeholder='请输入菜单名称'></Input>
        </Form.Item>
        <Form.Item shouldUpdate noStyle>
          {() => {
            return form.getFieldValue('menuType') === 2 ? (
              <Form.Item label='权限标识' name='menuCode'>
                <Input placeholder='请输入权限标识'></Input>
              </Form.Item>
            ) : (
              <>
                <Form.Item label='菜单图标' name='icon'>
                  <Input placeholder='请输入菜单图标'></Input>
                </Form.Item>
                <Form.Item label='路由地址' name='path'>
                  <Input placeholder='请输入路由地址'></Input>
                </Form.Item>
                <Form.Item label='组件地址' name='component'>
                  <Input placeholder='请输入组件地址'></Input>
                </Form.Item>
              </>
            );
          }}
        </Form.Item>
        <Form.Item
          label='排序'
          name='orderBy'
          tooltip={{
            title: '排序值越大越靠后',
            icon: <InfoCircleOutlined />
          }}
        >
          <InputNumber placeholder='请输入排序值'></InputNumber>
        </Form.Item>
        <Form.Item label='菜单状态' name='menuState'>
          <Radio.Group>
            <Radio value={1}>启用</Radio>
            <Radio value={2}>停用</Radio>
          </Radio.Group>
        </Form.Item>
      </Form>
    </Modal>
  );
}
