import { Form, Input, Button, Table, Space, Modal } from 'antd';
import { useForm } from 'antd/es/form/Form';
import { useEffect, useRef, useState } from 'react';
import { getDeptList, deleteDept } from '@/api';
import { Dept } from '@/types';
import CreateDept from './createDept';
import { IAction } from '@/types/modal';
import type { ColumnsType } from 'antd/es/table';
import { message } from '@/utils/AntdGlobal';
import { formatDate } from '@/utils';

export default function DeptList() {
  const [form] = useForm();
  const columns: ColumnsType<Dept.DeptItem> = [
    {
      title: '部门名称',
      dataIndex: 'deptName',
      key: 'deptName',
      width: 200
    },
    {
      title: '负责人',
      dataIndex: 'userName',
      key: 'userName',
      width: 150
    },
    {
      title: '更新时间',
      dataIndex: 'updateTime',
      key: 'updateTime',
      render(value: string) {
        return <span>{formatDate(value)}</span>;
      }
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      key: 'createTime',
      render(value: string) {
        return <span>{formatDate(value)}</span>;
      }
    },
    {
      title: '操作区',
      key: 'action',
      width: 200,
      render(record: Dept.EditorParams) {
        return (
          <Space>
            <Button type='text' onClick={handleSubCreate(record._id)}>
              新增
            </Button>
            <Button type='text' onClick={handleEditor(record)}>
              编辑
            </Button>
            <Button type='text' onClick={handleDelete(record._id)}>
              删除
            </Button>
          </Space>
        );
      }
    }
  ];
  const [data, setData] = useState<Dept.DeptItem[]>([]);
  const deptRef = useRef<{
    open: (
      type: IAction,
      data?: Dept.EditorParams | { parentId: string }
    ) => void;
  }>();
  useEffect(() => {
    getDeptData();
  }, []);
  const getDeptData = async () => {
    const data = await getDeptList(form.getFieldsValue());
    setData(data);
  };
  const handleReset = () => {
    form.resetFields();
  };
  const hanldeCreate = () => {
    deptRef.current?.open('create');
  };
  const handleEditor = (record: Dept.EditorParams) => () => {
    deptRef.current?.open('edit', record);
  };
  const handleSubCreate = (parentId: string) => () => {
    deptRef.current?.open('create', { parentId });
  };
  const handleDelete = (_id: string) => () => {
    Modal.confirm({
      title: '删除确认',
      content: <span>确定删除该部门吗?</span>,
      onOk: async () => {
        try {
          await deleteDept({ _id });
          message.success('删除成功');
          getDeptData();
        } catch (err) {
          message.error('删除失败');
          console.log(err);
        }
      }
    });
  };
  return (
    <div>
      <Form className='search-form' layout='inline' form={form}>
        <Form.Item label='部门名称' name='deptName'>
          <Input placeholder='请输入部门名称'></Input>
        </Form.Item>
        <Form.Item>
          <Button type='primary' className='mr10' onClick={getDeptData}>
            搜索
          </Button>
          <Button type='default' onClick={handleReset}>
            重置
          </Button>
        </Form.Item>
      </Form>
      <div className='base-table'>
        <div className='header-wrapper'>
          <div className='title'>部门列表</div>
          <div className='action'>
            <Button type='primary' onClick={hanldeCreate}>
              新增
            </Button>
          </div>
        </div>
        <Table
          bordered
          rowKey='_id'
          columns={columns}
          dataSource={data}
          pagination={false}
        ></Table>
      </div>
      <CreateDept mRef={deptRef} update={getDeptData}></CreateDept>
    </div>
  );
}
