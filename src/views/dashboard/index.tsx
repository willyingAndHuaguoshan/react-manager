import { Descriptions, Card, Button } from 'antd';
import type { DescriptionsProps } from 'antd';
import styles from './index.module.less';
import { useEffect, useState } from 'react';
import useStore from '@/store';
import { formatState, formatMoney, formatNum } from '@/utils';
import {
  getReportData,
  getLineData,
  getPieCityData,
  getPieAgeData,
  getRadarData
} from '@/api';
import { DashBoardTypes } from '@/types';
import { useEcharts } from '@/hook/useChart';

export default function DashBoard() {
  const userInfo = useStore(state => state.userInfo);
  const [reportData, setReportData] = useState<DashBoardTypes.reportData>();
  const [lineRef, lineChart] = useEcharts();
  const [pieCityRef, pieCityChart] = useEcharts();
  const [pieAgeRef, pieAgeChart] = useEcharts();
  const [radarRef, radarChart] = useEcharts();
  useEffect(() => {
    if (!lineChart || !pieCityChart || !pieAgeChart || !radarChart) return;
    renderLineChart();
    renderPieCityChart();
    renderPieAgeChart();
    renderRadarData();
  }, [lineChart, pieCityChart, pieAgeChart, radarChart]);

  const items: DescriptionsProps['items'] = [
    {
      key: 'userId',
      label: '用户ID',
      children: userInfo?.userId
    },
    {
      key: 'email',
      label: '邮箱',
      children: userInfo?.userEmail
    },
    {
      key: 'status',
      label: '状态',
      children: formatState(userInfo?.state as number)
    },
    {
      key: 'phoneNumber',
      label: '手机号',
      children: userInfo?.mobile
    },
    {
      key: 'post',
      label: '岗位',
      children: userInfo?.job
    },
    {
      key: 'section',
      label: '部门',
      children: userInfo?.deptName
    }
  ];

  useEffect(() => {
    getDashBoardReportData();
  }, []);

  const getDashBoardReportData = async () => {
    const data = await getReportData();
    setReportData(data);
  };

  const renderLineChart = async () => {
    const { label, money, order } = await getLineData();
    lineChart?.setOption({
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: ['订单', '流水']
      },
      // 控制尺寸
      grid: {
        left: '5%',
        right: '5%',
        bottom: '10%'
      },
      xAxis: {
        data: label
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          name: '订单',
          type: 'line',
          data: order
        },
        {
          name: '流水',
          type: 'line',
          data: money
        }
      ]
    });
  };

  const renderPieCityChart = async () => {
    const data = await getPieCityData();
    pieCityChart?.setOption({
      title: {
        text: '司机城市分布',
        left: 'center'
      },
      tooltip: {
        trigger: 'item'
      },
      legend: {
        orient: 'vertical',
        left: 'left'
      },
      series: [
        {
          name: '城市分布',
          type: 'pie',
          radius: '50%',
          data
        }
      ]
    });
  };
  const renderPieAgeChart = async () => {
    const data = await getPieAgeData();
    pieAgeChart?.setOption({
      title: {
        text: '司机年龄分布',
        left: 'center'
      },
      tooltip: {
        trigger: 'item'
      },
      legend: {
        orient: 'vertical',
        left: 'left'
      },
      series: [
        {
          name: '年龄分布',
          type: 'pie',
          radius: [50, 180], // 指定圆的半斤
          roseType: 'area',
          data
        }
      ]
    });
  };
  const renderRadarData = async () => {
    const { indicator, data } = await getRadarData();
    radarChart?.setOption({
      legend: {
        data: ['司机模型诊断']
      },
      radar: {
        indicator
      },
      series: [
        {
          name: '模型诊断',
          type: 'radar',
          data
        }
      ]
    });
  };
  // 刷新
  const handleRefresh = (type?: string) => {
    if (type === 'pie') {
      renderPieAgeChart();
      renderPieCityChart();
    } else if (type === 'radar') {
      renderRadarData();
    } else {
      renderLineChart();
    }
  };
  return (
    <div className={styles['dashboard-wrapper']}>
      <div className={styles['user-info']}>
        <img
          src='http://api-driver.marsview.cc/9fc8c95dd2a67d9d593625a79.jpeg'
          className={styles['user-avata']}
        />
        <Descriptions title='React18 通用后台管理系统' items={items} />
      </div>
      <div className={styles['report']}>
        <div className={styles['card']}>
          <div className='title'>司机数量</div>
          <div className={styles['data']}>
            {formatNum(reportData?.driverCount)}个
          </div>
        </div>
        <div className={styles['card']}>
          <div className='title'>总流水</div>
          <div className={styles['data']}>
            {formatMoney(reportData?.totalMoney)}元
          </div>
        </div>
        <div className={styles['card']}>
          <div className='title'>总订单</div>
          <div className={styles['data']}>
            {formatNum(reportData?.orderCount)}单
          </div>
        </div>
        <div className={styles['card']}>
          <div className='title'>开通城市</div>
          <div className={styles['data']}>
            {formatNum(reportData?.cityNum)}座
          </div>
        </div>
      </div>
      <div className={styles['chart']}>
        <Card
          title='订单和流水走势图'
          extra={
            <Button type='primary' onClick={() => handleRefresh()}>
              刷新
            </Button>
          }
        >
          <div
            id='line-chart'
            className={styles['item-line']}
            ref={lineRef}
          ></div>
        </Card>
      </div>
      <div className={styles['chart']}>
        <Card
          title='司机分布'
          extra={
            <Button type='primary' onClick={() => handleRefresh('pie')}>
              刷新
            </Button>
          }
        >
          <div className={styles['pie-chart']}>
            <div
              id='pie-chart-city'
              className={styles['item-line']}
              ref={pieCityRef}
            ></div>
            <div
              id='pie-chart-age'
              className={styles['item-line']}
              ref={pieAgeRef}
            ></div>
          </div>
        </Card>
      </div>
      <div className={styles['chart']}>
        <Card
          title='模型诊断'
          extra={
            <Button type='primary' onClick={() => handleRefresh('radar')}>
              刷新
            </Button>
          }
        >
          <div
            id='radar-chart'
            className={styles['item-line']}
            ref={radarRef}
          ></div>
        </Card>
      </div>
    </div>
  );
}
