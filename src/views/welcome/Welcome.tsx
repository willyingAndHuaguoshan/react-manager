import styles from './index.module.less';
export default function Welcome() {
  return (
    <div className={styles['welcome']}>
      <div className={styles['content']}>
        <div className={styles['subtitle']}>欢迎体验</div>
        <div className={styles['title']}>React18通用管理后台系统</div>
        <div className={styles['desc']}>
          React18+ReactRouter6.0+AntD5.4+TypeScript
        </div>
      </div>
      <div className={styles['img']}></div>
    </div>
  );
}
