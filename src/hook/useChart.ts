import * as echarts from 'echarts';
import { RefObject, useEffect, useRef, useState } from 'react';
export const useEcharts = (): [
  RefObject<HTMLDivElement>,
  echarts.EChartsType | undefined
] => {
  const chartsRef = useRef<HTMLDivElement>(null);
  const [chartsInstance, setChartsInstance] = useState<echarts.EChartsType>();
  useEffect(() => {
    const chart = echarts.init(chartsRef.current as HTMLDivElement);
    setChartsInstance(chart);
  }, []);
  return [chartsRef, chartsInstance];
};
