import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import { Breadcrumb, Switch, Dropdown } from 'antd';
import type { MenuProps } from 'antd';
import styles from './index.module.less';
import useStore from '@/store';
import storage from '@/utils/storage';
import { useNavigate } from 'react-router-dom';
const NavHeader = () => {
  const { userInfo, collapsed, updateCollapse } = useStore();
  const navigate = useNavigate();
  const breadList = [
    {
      title: '首页'
    },
    {
      title: '系统管理'
    }
  ];
  const items: MenuProps['items'] = [
    {
      key: 'email',
      label: '邮箱：' + userInfo?.userEmail
    },
    {
      key: 'logout',
      label: '退出'
    }
  ];
  const onClick: MenuProps['onClick'] = ({ key }) => {
    if (key === 'logout') {
      storage.remove('token');
      setTimeout(() => {
        navigate(`/login?callback=${encodeURIComponent(location.href)}`);
      });
    }
  };
  return (
    <div className={styles['nav-header']}>
      <div className={styles['left']}>
        <div onClick={updateCollapse} className={styles['collapse']}>
          {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
        </div>
        <Breadcrumb items={breadList} style={{ marginLeft: 10 }}></Breadcrumb>
      </div>
      <div className={styles['right']}>
        <Switch
          checkedChildren='暗黑'
          unCheckedChildren='默认'
          style={{ marginRight: 10 }}
        ></Switch>
        <Dropdown menu={{ items, onClick }} trigger={['click']}>
          <a onClick={e => e.preventDefault()}>{userInfo?.userName}</a>
        </Dropdown>
      </div>
    </div>
  );
};
export default NavHeader;
