import styles from './index.module.less';
const NavFooter = () => {
  return (
    <div className={styles['nav-footer']}>
      <div>
        <a
          href='https://blog.csdn.net/qq_45572424?spm=1000.2115.3001.5343'
          target='_blank'
          rel='noreferrer'
        >
          Will主页
        </a>
        <span className={styles['gutter']}>|</span>
        <a href='#' target='_blank' rel='noreferrer'>
          React18+Typescript+Vite开发通用后台管理系统
        </a>
        <span className={styles['gutter']}>|</span>
        <a
          href='https://gitee.com/willyingAndHuaguoshan'
          target='_blank'
          rel='noreferrer'
        >
          Gitee主页
        </a>
      </div>
    </div>
  );
};
export default NavFooter;
