import { Button } from 'antd';
import { useRouteLoaderData } from 'react-router-dom';
import { IuseRouterData } from '@/types';
import useStore from '@/store';
export default function AuthButton(props: any) {
  const role = useStore(state => state.userInfo?.role);
  const data = useRouteLoaderData('layout') as IuseRouterData;
  if (!props.auth) return <Button {...props}>{props.children}</Button>;
  if (data.buttonList.includes(props.auth) || role === 1) {
    return <Button {...props}>{props.children}</Button>;
  }
  return <></>;
}
