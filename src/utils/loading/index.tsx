import ReactDOM from 'react-dom/client';
import Loading from './loading';
import './loading.less';

let count = 0;
export const ShowLoading = () => {
  if (count === 0) {
    const loading = document.createElement('div');
    loading.setAttribute('id', 'loading');
    document.body.appendChild(loading);
    ReactDOM.createRoot(loading).render(<Loading></Loading>);
  }
  count++;
};

export const HidenLoading = () => {
  if (count < 0) return;
  count--;
  if (count === 0) {
    document.body.removeChild(document.getElementById('loading')!);
  }
};
