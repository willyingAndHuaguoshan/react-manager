export default {
  /**
   * storage存储
   * @params key {string} 参数名称
   * @params value {T} 写入值
   */
  set<T>(key: string, value: T) {
    if (typeof value === 'string') {
      localStorage.setItem(key, value);
    } else if (typeof value === 'object' && value !== null) {
      localStorage.setItem(key, JSON.stringify(value));
    } else {
      new Error('localStorage 只能存储字符串或对象');
    }
  },
  get(key: string) {
    const value = localStorage.getItem(key);
    try {
      return JSON.parse(value!);
    } catch (e) {
      return value;
    }
  },
  /**
   * 删除localstorage值
   * @params key {string} 参数名称
   */
  remove(key: string) {
    localStorage.removeItem(key);
  },
  /**
   * 清空localstorage值
   */
  clear() {
    localStorage.clear();
  }
};
