import request from '@/utils/request';
import {
  login,
  User,
  DashBoardTypes,
  ResultData,
  Dept,
  Menu,
  Role,
  Order
} from '@/types';

export const loginRequst = (data: login.params) => {
  return request.post('/users/login', data, {
    showError: true,
    showLoading: true
  });
};

export const getUserInfo = () => {
  return request.get<User.UserItem>('/users/getUserInfo');
};

export const getReportData = () => {
  return request.get<DashBoardTypes.reportData>(
    '/order/dashboard/getReportData'
  );
};

export const getLineData = () => {
  return request.get<DashBoardTypes.lineData>('/order/dashboard/getLineData');
};

export const getPieCityData = () => {
  return request.get<DashBoardTypes.pieData[]>(
    '/order/dashboard/getPieCityData'
  );
};

export const getPieAgeData = () => {
  return request.get<DashBoardTypes.pieData[]>(
    '/order/dashboard/getPieAgeData'
  );
};

export const getRadarData = () => {
  return request.get<DashBoardTypes.radarData>('/order/dashboard/getRadarData');
};

export const getUserList = (params?: User.Params) => {
  return request.get<ResultData<User.UserItem>>('/users/list', params);
};

export const createUser = (params: User.CreateParams) => {
  return request.post('/users/create', params);
};

export const editorUser = (params: User.EditorParams) => {
  return request.post('/users/edit', params);
};

export const deleteUser = (params: User.DeleteParams) => {
  return request.post('/users/delete', params);
};

export const getDeptList = (params?: Dept.Params) => {
  return request.get<Dept.DeptItem[]>('/dept/list', params);
};

// 获取当前账号下的所有用户
export const getAllUserList = () => {
  return request.get<User.UserItem[]>('/users/all/list');
};

export const createDept = (params: Dept.CreateParams) => {
  return request.post('/dept/create', params);
};

export const editDept = (params: Dept.EditorParams) => {
  return request.post('/dept/edit', params);
};

export const deleteDept = (params: { _id: string }) => {
  return request.post('/dept/delete', params);
};

// 菜单管理
export const getMenuList = (params?: Menu.Params) => {
  return request.get<Menu.MenuItem[]>('/menu/list', params);
};

// 创建菜单
export const createMenu = (params: Menu.CreateParams) => {
  return request.post('/menu/create', params);
};

// 编辑菜单
export const editMenu = (params: Menu.EditParams) => {
  return request.post('/menu/edit', params);
};

//删除菜单
export const deleteMenu = (params: Menu.DeleteParams) => {
  return request.post('/menu/delete', params);
};

// 获取权限列表
export const getPermissionList = () => {
  return request.get<{
    buttonList: string[];
    menuList: Menu.MenuItem[];
  }>('/users/getPermissionList');
};

export const getRoleList = (params: Role.Params) => {
  return request.get<ResultData<Role.RoleItem>>('/roles/list', params);
};

export const createRole = (params: Role.CreateParams) => {
  return request.post('/roles/create', params);
};

export const editRole = (params: Role.CreateParams) => {
  return request.post('/roles/edit', params);
};

export const deleteRole = (params: Role.DeleteParams) => {
  return request.post('/roles/delete', params);
};
// 设置权限
export const updatePermission = (params: Role.Permission) => {
  return request.post('/roles/update/permission', params);
};

export const getAllRoleList = () => {
  return request.get<Role.RoleItem[]>('/roles/allList');
};

// 订单列表
export const getOrderList = (params: Order.Params) => {
  return request.get<ResultData<Order.OrderItem>>('/order/list', params);
};

// 删除订单
export const deleteOrder = (params: Order.DeleteParams) => {
  return request.post('/order/delete', params);
};

// 获取城市
export const getCityList = () => {
  return request.get<Order.CityItem[]>('/order/cityList');
};

// 获取车型
export const getCarTypeList = () => {
  return request.get<Order.VehicleItem[]>('/order/vehicleList');
};

// 创建订单
export const createOrder = (params: Order.CreateParams) => {
  return request.post('/order/create', params);
};

// 订单详情
export const orderDetail = (orderId: string) => {
  return request.get<Order.OrderItem>(`/order/detail/${orderId}`);
};

// 提交打点
export const updateOrderInfo = (params: Order.OrderEdit) => {
  return request.post('/order/edit', params);
};

export const exportOrder = (params: Order.SearchParams) => {
  return request.downLoadFile('/order/orderExport', params);
};
