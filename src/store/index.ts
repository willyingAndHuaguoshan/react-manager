import { create } from 'zustand';
import { User } from '@/types';
const useStore = create<{
  token: string;
  userInfo: User.UserItem | null;
  collapsed: boolean;
  updataUserInfo(userInfo: User.UserItem): void;
  updateToken(token: string): void;
  updateCollapse(): void;
}>(set => ({
  token: '',
  userInfo: null,
  collapsed: false,
  updataUserInfo: (userData: User.UserItem) => set({ userInfo: userData }),
  updateToken: token => set({ token }),
  updateCollapse: () => set(state => ({ collapsed: !state.collapsed }))
}));

export default useStore;
