/**
 * 环境配置封装
 */

type ENV = 'dev' | 'prod' | 'stg';
let env: ENV = 'dev';
if (location.host.indexOf('localhost') > -1) {
  env = 'dev';
} else if (location.host === 'driver-stg.marsview.cc') {
  env = 'stg';
} else {
  env = 'prod';
}

const config = {
  dev: {
    baseApi: '/api',
    uploadApi: 'http://api-driver-dev.marsview.cc',
    mock: false,
    mockApi:
      'https://www.fastmock.site/mock/667777fb0b161cdf0cf56f5c0db0304a/api',
    cdn: 'http://www.aliyun.com'
  },
  stg: {
    baseApi: 'http://api-driver-stg.marsview.cc/api',
    uploadApi: 'http://api-driver-stg.marsview.cc',
    mock: false,
    mockApi:
      'https://www.fastmock.site/mock/667777fb0b161cdf0cf56f5c0db0304a/api',
    cdn: 'http://www.aliyun.com'
  },
  prod: {
    baseApi: 'http://api-driver.marsview.cc',
    uploadApi: 'http://api-driver.marsview.cc',
    mock: false,
    mockApi:
      'https://www.fastmock.site/mock/667777fb0b161cdf0cf56f5c0db0304a/api',
    cdn: 'http://www.aliyun.com'
  }
};

export default {
  env,
  ...config[env]
};
