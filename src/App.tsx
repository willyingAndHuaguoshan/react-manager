import router from './router';
import { RouterProvider } from 'react-router';
import { ConfigProvider, App as AntdApp } from 'antd';
import AntdGlobal from './utils/AntdGlobal';
import './App.less';
function App() {
  return (
    <ConfigProvider
      theme={{
        token: {
          colorPrimary: '#ed6c00'
        }
      }}
    >
      <AntdApp>
        <AntdGlobal></AntdGlobal>
        <RouterProvider router={router}></RouterProvider>
      </AntdApp>
    </ConfigProvider>
  );
}
export default App;
