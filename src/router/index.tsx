import { Navigate, createBrowserRouter } from 'react-router-dom';
import Login from '@/views/login/Login';
import Error403 from '@/views/403';
import Error404 from '@/views/404';
import DashBoard from '@/views/dashboard';
import User from '@/views/system/user';
import ManagerLayout from '@/layout';
import Welcome from '@/views/welcome/Welcome';
import DeptList from '@/views/system/dept';
import MenuList from '@/views/system/menu';
import AuthLoader from './AuthLoader';
import RoleList from '@/views/system/role';
import OrderList from '@/views/order/orderList';

export const router = [
  {
    path: '/',
    element: <Navigate to='/welcome' />
  },
  {
    path: '/login',
    element: <Login></Login>
  },
  {
    path: '/404',
    element: <Error404></Error404>
  },
  {
    id: 'layout',
    element: <ManagerLayout></ManagerLayout>,
    loader: AuthLoader,
    children: [
      {
        path: '/welcome',
        element: <Welcome></Welcome>,
        meta: {
          permission: false
        }
      },
      {
        path: '/dashboard',
        element: <DashBoard></DashBoard>,
        meta: {
          permission: false
        }
      },
      {
        path: '/userlist',
        element: <User></User>
      },
      {
        path: '/deptlist',
        element: <DeptList></DeptList>
      },
      {
        path: '/menulist',
        element: <MenuList></MenuList>
      },
      {
        path: '/rolelist',
        element: <RoleList></RoleList>
      },
      {
        path: '/orderList',
        element: <OrderList></OrderList>
      }
    ]
  },
  {
    path: '/403',
    element: <Error403></Error403>
  },
  {
    path: '*',
    element: <Navigate to={'/404'}></Navigate>
  }
];

// export default function Router() {
//   return useRoutes(router);
// }

export default createBrowserRouter(router);
