import { getPermissionList } from '@/api';
import { getMenuPath } from '@/utils';
export default async function AuthLoader() {
  const { buttonList, menuList } = await getPermissionList();
  const menuPathList = getMenuPath(menuList);
  return {
    buttonList,
    menuList,
    menuPathList
  };
}
