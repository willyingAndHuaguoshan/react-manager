declare interface Window {
  BMapGL: {
    [propName: string]: any;
  };
  BMapGLLib: {
    [propName: string]: any;
  };
}
