// 接口类型定义
export interface Result<T = any> {
  code: number;
  data: T;
  msg: string;
}
export interface ResultData<T = any> {
  list: T[];
  page: {
    pageNum: number;
    pageSize: number;
    total: number | 0;
  };
}
export interface PageParams {
  pageNum: number;
  pageSize?: number;
}
export namespace login {
  export interface params {
    userName: string;
    userPwd: string;
  }
}
export namespace User {
  export interface Params extends PageParams {
    userId?: number;
    userName?: string;
    state?: number;
  }
  export interface UserItem {
    createId: number;
    deptId: string;
    deptName: string;
    job: string;
    mobile: string;
    role: number;
    roleList: any[];
    state: number;
    userEmail: string;
    userId: number;
    userImg: string;
    userName: string;
    _id: string;
  }
  export interface CreateParams {
    userName: string;
    userEmail: string;
    mobile?: number;
    deptId: string;
    job?: string;
    state?: number;
    roleList: string[];
    userImg: string;
  }
  export interface EditorParams extends CreateParams {
    userId: string;
  }
  export interface DeleteParams {
    userIds: number[];
  }
}

export namespace Dept {
  export interface Params {
    deptName?: string;
  }
  export interface DeptItem {
    _id: string;
    createTime: string;
    updateTime: string;
    deptName: string;
    parentId: string;
    username: string;
    children: DeptItem[];
  }
  export interface CreateParams {
    deptName: string;
    parentId?: string;
    userName: string;
  }
  export interface EditorParams extends CreateParams {
    _id: string;
  }
}
export namespace DashBoardTypes {
  export interface reportData {
    driverCount: number;
    totalMoney: number;
    orderCount: number;
    cityNum: number;
  }

  export interface lineData {
    label: string;
    money: number;
    order: number;
  }

  export interface pieData {
    value: number;
    name: string;
  }

  export interface radarData {
    indicator: Array<{ name: string; max: number }>;
    data: {
      name: string;
      value: number[];
    }[];
  }
}

export namespace Menu {
  export interface Params {
    menuName: string;
    menuState: number; // 1正常 2停用
  }
  export interface CreateParams {
    menuName: string; //菜单名称
    icon?: string; //菜单图标
    menuType: number; // 1.菜单 2.按钮 3.页面
    menuState: number; // 1.正常 2.停用
    menuCode?: string; // 按钮权限表示
    parentId?: string; // 父级菜单id
    path?: string; // 菜单路径
    component?: string; //组件名称
    orderBy?: number;
  }
  export interface EditParams extends CreateParams {
    _id: string;
  }
  export interface DeleteParams {
    _id: string;
  }
  export interface MenuItem extends CreateParams {
    _id: string;
    createTime: string;
    buttons?: MenuItem[];
    children?: MenuItem[];
  }
}

export namespace Role {
  export interface Params extends PageParams {
    roleName?: string;
  }
  export interface CreateParams {
    roleName: string; //角色名称
    remark?: string; // 备注
  }
  export interface RoleItem extends CreateParams {
    _id: string;
    permissionList: {
      checkedKeys: string[];
      halfCheckedKeys: string[];
    };
    updateTime: string;
    createTime: string;
  }
  export interface EditorParams extends CreateParams {
    _id: string;
  }
  export interface DeleteParams {
    _id: string;
  }
  export interface Permission {
    _id: string;
    permissionList: {
      checkedKeys: string[];
      halfCheckedKeys: string[];
    };
  }
}

export interface IuseRouterData {
  buttonList: string[];
  menuList: Menu.MenuItem[];
  menuPathList: string[];
}

export namespace Order {
  export enum IState {
    doing = 1,
    done = 2,
    timeout = 3,
    cance = 4
  }
  export interface CreateParams {
    cityName: string;
    userName: string;
    mobile: number;
    startAddress: string;
    endAddress: string;
    userPayAmount: number;
    dirverAmount: number;
    payType: number;
    driverName: string;
    vehicleName: string; // 订单车型
    state: IState; // 订单状态
    useTime: string;
    endTime: string;
    orderAmount: string;
  }
  export interface OrderItem extends CreateParams {
    _id: string;
    createTime: string;
    orderId: string; // 订单ID
    route: Array<{ lng: number; lat: number }>; // 行驶轨迹
    remark: string; //备注
  }
  export interface SearchParams {
    orderId?: string;
    userName?: string;
    state?: IState;
  }
  export interface Params extends PageParams {
    orderId?: string;
    userName?: string;
    state?: IState;
  }
  export interface DeleteParams {
    _id: string;
  }
  export interface CityItem {
    id: number;
    name: string;
  }
  export interface VehicleItem {
    id: number;
    name: string;
  }
  export interface OrderEdit {
    orderId: string;
    route: Array<{ lng: number; lat: number }>;
  }
}
