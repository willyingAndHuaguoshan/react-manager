import { MutableRefObject } from 'react';
import { User } from '.';

export type IAction = 'create' | 'edit' | 'delete';

export interface IModalProps<T = User.UserItem> {
  mRef: MutableRefObject<
    { open: (type: IAction, data: T) => void } | undefined
  >;
  update: () => void;
}

export interface IModalDetailProps {
  mRef: MutableRefObject<{ open: (orderId: string) => void } | undefined>;
}
